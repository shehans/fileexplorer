package com.folder.explorer.repository;

import com.folder.explorer.entity.FileItem;
import org.springframework.data.neo4j.repository.Neo4jRepository;

import java.util.List;

/**
 * Repository class to do GraphDB operations.
 * save, delete methods are inherited from the parent class
 */

public interface FileItemRepository extends Neo4jRepository<FileItem, Long> {
    FileItem findByName(String name);
    FileItem findRootNode();
    List<FileItem> findChildren(Long id);
    FileItem findParent(Long id);
}
