package com.folder.explorer.entity;

import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.io.File;
import java.util.Set;

/**
 * This represent a node of the graph.
 * FileItem can be a directory or a file.
 * FileItem has a parent and multiple children.
 * The root node will represent the whole directory structure.
 */
@NodeEntity
public class FileItem {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    private Boolean isDirectory;

    private String filePath;

    private File file;

    @Relationship(type = "PARENT", direction = Relationship.INCOMING)
    private FileItem parent;

    @Relationship(type = "CHILDREN", direction = Relationship.OUTGOING)
    private Set<FileItem> children;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getDirectory() {
        return isDirectory;
    }

    public void setDirectory(Boolean directory) {
        isDirectory = directory;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public FileItem getParent() {
        return parent;
    }

    public void setParent(FileItem parent) {
        this.parent = parent;
    }

    public Set<FileItem> getChildren() {
        return children;
    }

    public void setChildren(Set<FileItem> children) {
        this.children = children;
    }

    public Long getId() {
        return id;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    @Override
    public String toString() {
        return "FileItem{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", isDirectory=" + isDirectory +
                ", filePath='" + filePath + '\'' +
                ", parent=" + parent +
                ", children=" + children +
                '}';
    }
}
