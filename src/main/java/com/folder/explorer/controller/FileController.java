package com.folder.explorer.controller;

import com.folder.explorer.entity.FileItem;
import com.folder.explorer.service.ExplorerService;
import org.neo4j.cypher.internal.frontend.v2_3.ast.functions.Str;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.Mapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;

@RestController
@RequestMapping("/file")
public class FileController {

    private ExplorerService explorerService;

    public FileController(ExplorerService explorerService) {
        this.explorerService = explorerService;
    }

    @PostMapping("/create")
    public ResponseEntity<String> createFileItem(@RequestParam String fileName,
                                                 @RequestParam(required = false) File file,
                                                 @RequestParam(required = false) Long parentId) {
        explorerService.createFileItem(fileName, file, parentId);
        return ResponseEntity.ok("FileItem created");
    }

    /**
     * This will return the entire folder structure (root node)
     * @return
     */
    @GetMapping("/root")
    public ResponseEntity<FileItem> getRoot() {
        FileItem root = explorerService.getRoot();
        return ResponseEntity.ok(root);
    }

    @DeleteMapping("/delete")
    public ResponseEntity<String> deleteFile(@RequestParam Long id) {
        explorerService.delete(id);
        return ResponseEntity.ok("FileItem deleted");
    }

}
