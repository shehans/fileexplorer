package com.folder.explorer.service;

import com.folder.explorer.entity.FileItem;
import com.folder.explorer.repository.FileItemRepository;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.Result;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.util.Optional;

@Service
public class ExplorerService {

    private FileItemRepository fileItemRepository;

    private GraphDatabaseService graphDBService;

    public ExplorerService(FileItemRepository fileItemRepository) {
        this.fileItemRepository = fileItemRepository;
    }

    @PostConstruct
    public void init() {
        GraphDatabaseFactory graphDbFactory = new GraphDatabaseFactory();
        graphDBService = graphDbFactory.newEmbeddedDatabase(
                new File("data/explorer"));
        graphDBService.beginTx();
    }

    /**
     * If parentId is null, then this is the root file item
     * @param fileName
     * @param file
     * @param parentId
     */
    public void createFileItem(String fileName, File file, Long parentId) {

        FileItem fileItem = new FileItem();
        fileItem.setName(fileName);
        fileItem.setDirectory(file == null);
        Optional<FileItem> parentFileItemOp = fileItemRepository.findById(parentId);
        fileItem.setParent(parentFileItemOp.get());

        fileItemRepository.save(fileItem);
    }

    public FileItem getRoot() {
        return fileItemRepository.findRootNode();
    }

    /**
     * delete fileItem including it's children
     * @param id
     */
    public void delete(Long id) {
        fileItemRepository.deleteById(id);
    }
}
